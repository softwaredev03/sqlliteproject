/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teerarak.spliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AuyouknoW
 */
public class ConnectDataBase {

    public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
        } catch (ClassNotFoundException ex) {
            System.out.println("Libraly org.sqlite.JDBC not found!!!");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to open Database!!");
        }
        System.out.println("Open Database Successful");
    }

}
