/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teerarak.spliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AuyouknoW
 */
public class UpdateUser {
     public static void main(String[] args) throws SQLException {
        Connection conn = null;
        Statement stmt = null;
        String dbName = "user.db";
        
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            stmt = conn.createStatement();
            
            //UPDATE
            stmt.executeUpdate("UPDATE USER set password = password1 where ID = 1");
            
            
            
            
            
            
            //SELECT
            ResultSet rs = stmt.executeQuery("SELECT * FROM USER");
            
            while(rs.next()){
                int id = rs.getInt("id");
                String username = rs.getString("username");
                int password = rs.getInt("password");
                System.out.println("ID ="+id);
                System.out.println("USERNAME ="+username);
                System.out.println("PASSWORD"+password);
                
            }
                    
            
            rs.close();
            stmt.close();
            conn.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
